package com.learnplatform.demo.after.config;

import com.octo.captcha.engine.CaptchaEngine;
import com.octo.captcha.service.captchastore.CaptchaStore;
import com.octo.captcha.service.image.DefaultManageableImageCaptchaService;
import org.springframework.stereotype.Service;


/**
 * Created by 26645 on 2018/1/2.
 */

public class CustomManageableImageCaptchaService extends DefaultManageableImageCaptchaService {

    public CustomManageableImageCaptchaService(
           CaptchaStore captchaStore,
            CaptchaEngine captchaEngine,
            int minGuarantedStorageDelayInSeconds,
            int maxCaptchaStoreSize,
            int captchaStoreLoadBeforeGarbageCollection) {
        super(captchaStore, captchaEngine, minGuarantedStorageDelayInSeconds,
                maxCaptchaStoreSize, captchaStoreLoadBeforeGarbageCollection);
    }
    public boolean hasCapcha(String id, String userCaptchaResponse) {
        return store.getCaptcha(id).validateResponse(userCaptchaResponse);
    }

}
