package com.learnplatform.demo.constant.api;

import com.learnplatform.demo.before.service.MyManageableImageCaptchaService;
import com.octo.captcha.engine.GenericCaptchaEngine;
import com.octo.captcha.engine.image.gimpy.BasicGimpyEngine;
import com.octo.captcha.service.captchastore.FastHashMapCaptchaStore;

/**
 * Created by 26645 on 2018/1/2.
 */


public class JCaptcha {

     /**
       * @author complone
       * @description:
       * @param captcha picture generate
       * @param captcha generic engine
       * @param
       * @date 2018/1/2 14:28
       */
    public static final MyManageableImageCaptchaService captchaSerivce =
             new MyManageableImageCaptchaService(new FastHashMapCaptchaStore(),new BasicGimpyEngine(),
                     180,100000,75000);


}
