package com.learnplatform.demo.constant.web;

import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by 26645 on 2018/1/2.
 */
public class ImageCaptchaServlet extends HttpServlet {

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpSession session = ((HttpServletRequest) request).getSession();
        ServletContext servletContext = session.getServletContext();


        chain.doFilter(request, response);
    }
}
