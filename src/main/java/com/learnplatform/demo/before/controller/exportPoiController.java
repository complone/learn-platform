package com.learnplatform.demo.before.controller;

import com.learnplatform.demo.before.model.exportMessage;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.junit.jupiter.api.Test;
import org.springframework.util.ResourceUtils;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 26645 on 2017/12/26.
 */
public class exportPoiController {


    public void Read() throws Exception {

        //Excel Files
        HSSFWorkbook wb = new HSSFWorkbook(new FileInputStream(ResourceUtils.getFile("classpath:web-info.xls")));

        //Excel WorkSheeet
        HSSFSheet sheet = wb.getSheetAt(0);

        HSSFRow titleRow = sheet.getRow(0);

        //表头那个单元格
        HSSFCell titleCell = titleRow.getCell(0);

        String title = titleCell.getStringCellValue();

        System.out.println("标题是："+title);

    }


    public void ReadList() throws Exception {
        List<exportMessage> list = new ArrayList<exportMessage>();

        HSSFWorkbook book = new HSSFWorkbook(new FileInputStream(ResourceUtils.getFile("classpath:web-info.xls")));

        HSSFSheet sheet = book.getSheetAt(0);

        for(int i=2; i<sheet.getLastRowNum()+1; i++) {
            HSSFRow row = sheet.getRow(i);
            String name = row.getCell(0).getStringCellValue(); //名称
            String url = row.getCell(1).getStringCellValue(); //url
            String username = row.getCell(2).getStringCellValue();
            String password = row.getCell(3).getStringCellValue();
            Integer readCount = (int) row.getCell(4).getNumericCellValue();

            list.add(new exportMessage(name, url, username, password, readCount));
        }

        System.out.println("共有 " + list.size() + " 条数据：");
        for(exportMessage wd : list) {
            System.out.println(wd);
        }
    }

}
