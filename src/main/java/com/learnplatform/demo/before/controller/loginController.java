package com.learnplatform.demo.before.controller;

/**
 * Created by 26645 on 2018/1/2.
 */

import com.learnplatform.demo.before.service.MyManageableImageCaptchaService;
import com.learnplatform.demo.before.util.captchaEngine;
import com.learnplatform.demo.before.util.jCaptcha;
import com.octo.captcha.service.captchastore.FastHashMapCaptchaStore;
import com.octo.captcha.service.image.ImageCaptchaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;

/**
   * @author complone
   * @description:  finish user require info
   * @date 2018/1/2 16:39
   */
 @Controller

public class loginController {

    @RequestMapping(value = "/welcome",method = RequestMethod.GET)
    public String returnLogin(){
        return "login";
    }


    @RequestMapping(value = "/login",method = RequestMethod.POST)
    public String login(@RequestParam("username") String username, @RequestParam("password") String password,
                        @RequestParam("captcha") String captcha, @RequestParam("error") String error,
                        Model model, HttpServletRequest request) {


        boolean isResponseCorrect = jCaptcha.captchaService.validateResponseForID(request.getSession().getId(), captcha);

        if (isResponseCorrect) {
            if (username.equals("admin") && password.equals("admin")) {
                model.addAttribute("username", username);
                return "home";
            } else {
                model.addAttribute("username", username);
                return "login";
            }
        } else {
            model.addAttribute("error", error);
            return "login";
        }
    }



}
