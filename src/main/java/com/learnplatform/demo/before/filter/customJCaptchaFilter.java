package com.learnplatform.demo.before.filter;

import com.learnplatform.demo.before.util.capchaHelper;

import com.octo.captcha.Captcha;
import com.octo.captcha.engine.CaptchaEngine;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.imageio.ImageIO;
import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by 26645 on 2018/1/2.
 */
 /**
   * @author complone
   * @description: 自定义验证码背景图片
   * @date 2018/1/2 15:05
   */
public class customJCaptchaFilter extends OncePerRequestFilter {

     @Override
     protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
         HttpSession session = ((HttpServletRequest) httpServletRequest).getSession();
         ServletContext servletContext = session.getServletContext();

         String captcha_backgrounds = session.getServletContext().getRealPath("classpath:/static/img/koouli.jpg");
         CaptchaEngine backgrounds_ce = capchaHelper.getCaptchaEngine(captcha_backgrounds);
         Captcha captcha = backgrounds_ce.getNextCaptcha();
         session.setAttribute("captcha",captcha);
         BufferedImage image = (BufferedImage)captcha.getChallenge();
         ByteArrayOutputStream bao = new ByteArrayOutputStream();
         ImageIO.write(image,"jpg",bao);
         try {
             bao.flush();
         }finally {
             bao.close();
         }
     }


}
