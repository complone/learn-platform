package com.learnplatform.demo.before.filter;

/**
 * Created by 26645 on 2018/1/2.
 */

import com.learnplatform.demo.before.service.MyManageableImageCaptchaService;
import com.learnplatform.demo.before.util.captchaEngine;
import com.learnplatform.demo.before.util.jCaptcha;
import com.octo.captcha.service.captchastore.FastHashMapCaptchaStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.imageio.ImageIO;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;


/**
   * @author complone
   * @description: CaptchaService 使用当前sessionId当做key获取相应的验证码图片,另外需要设置
   * @date 2018/1/2 15:17
   */

public class jCaptchaFilter extends OncePerRequestFilter {


 @Override
 protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
                                 FilterChain filterChain) throws ServletException, IOException {
  response.setDateHeader("Expires", 0L);
  response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
  response.addHeader("Cache-Control", "post-check=0, pre-check=0");
  response.setHeader("Pragma", "no-cache");
  response.setContentType("image/jpeg");
  String id = request.getRequestedSessionId();
  BufferedImage bi = jCaptcha.captchaService.getImageChallengeForID(id);
  ServletOutputStream out = response.getOutputStream();
  ImageIO.write(bi, "jpg", out);
  try {
   out.flush();
  } finally {
   out.close();
  }

 }
}
