package com.learnplatform.demo.before.model;

/**
 * Created by 26645 on 2017/12/26.
 */

public class exportMessage {

    //web name
    private String name;

    //web url
    private String url;

    //username
    private String username;

    //password
    private String password;

    //average numbers
    private Integer readCount;

    public exportMessage(String name, String url, String username, String password, Integer readCount) {
        this.name = name;
        this.url = url;
        this.username = username;
        this.password = password;
        this.readCount = readCount;
    }

    public exportMessage(){}

    @Override
    public String toString() {
        return "WebDto{" +
                "name='" + name + '\'' +
                ", url='" + url + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", readCount=" + readCount +
                '}';
    }


}
