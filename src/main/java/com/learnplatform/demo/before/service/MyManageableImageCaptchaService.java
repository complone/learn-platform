package com.learnplatform.demo.before.service;
import com.octo.captcha.engine.CaptchaEngine;
import com.octo.captcha.service.image.DefaultManageableImageCaptchaService;
import org.springframework.stereotype.Service;
import com.octo.captcha.service.captchastore.CaptchaStore;


/**
 * Created by 26645 on 2018/1/2.
 */

public class MyManageableImageCaptchaService extends DefaultManageableImageCaptchaService {

    public MyManageableImageCaptchaService(
           CaptchaStore captchaStore,
            CaptchaEngine captchaEngine,
            int minGuarantedStorageDelayInSeconds,
            int maxCaptchaStoreSize,
            int captchaStoreLoadBeforeGarbageCollection) {
        super(captchaStore, captchaEngine, minGuarantedStorageDelayInSeconds,
                maxCaptchaStoreSize, captchaStoreLoadBeforeGarbageCollection);
    }
    public boolean hasCapcha(String id, String userCaptchaResponse) {
        return store.getCaptcha(id).validateResponse(userCaptchaResponse);
    }

}
