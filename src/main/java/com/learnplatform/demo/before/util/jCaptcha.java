package com.learnplatform.demo.before.util;

import com.learnplatform.demo.before.service.MyManageableImageCaptchaService;
import com.octo.captcha.service.CaptchaServiceException;
import com.octo.captcha.service.captchastore.FastHashMapCaptchaStore;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by 26645 on 2018/1/2.
 */


public class jCaptcha {



     /**
       * @author complone
       * @description:
       * @param captcha picture generate
       * @param captcha generic engine
       * @param
       * @date 2018/1/2 14:28
       */
     public static final MyManageableImageCaptchaService captchaService
             = new MyManageableImageCaptchaService(new FastHashMapCaptchaStore(),
             new captchaEngine(), 180, 100000, 75000);
    public static boolean validateResponse(
            HttpServletRequest request, String userCaptchaResponse) {
        if (request.getSession(false) == null) return false;
        boolean validated = false;
        try {
            String id = request.getSession().getId();
            validated =
                    captchaService.validateResponseForID(id, userCaptchaResponse)
                            .booleanValue();
        } catch (CaptchaServiceException e) {
            e.printStackTrace();
        }
        return validated;
    }
    public static boolean hasCaptcha(
            HttpServletRequest request, String userCaptchaResponse) {
        if (request.getSession(false) == null) return false;
        boolean validated = false;
        try {
            String id = request.getSession().getId();
            validated = captchaService.hasCapcha(id, userCaptchaResponse);
        } catch (CaptchaServiceException e) {
            e.printStackTrace();
        }
        return validated;
    }


}
