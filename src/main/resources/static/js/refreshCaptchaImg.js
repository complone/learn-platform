/**
 * Created by 26645 on 2018/1/2.
 */
function refreshCaptchaImg() {
    //从服务端重新下载验证码图片
    //给这个地加参数纯为了强制刷新，否则由于src指向的url地址没变,浏览器不会真正生刷新图片
    var now = new Date()
    $("#captcha").attr("src", "/captcha_img?" + now.getTime());
}
